import 'package:siies/models/speakers.dart';
import 'package:siies/ui/common/card_speaker.dart';
import 'package:siies/ui/detail/detail_speaker.dart';
import 'package:flutter/material.dart';

class SpeakersPage extends StatefulWidget {
  @override
  _SpeakersPageState createState() => _SpeakersPageState();
}

class _SpeakersPageState extends State<SpeakersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Palestrantes".toUpperCase()),
          centerTitle: true,
        ),
        body: Container(
          child: Theme(
            data: Theme.of(context).copyWith(accentColor: Colors.grey),
            child: ListView.builder(
              padding: EdgeInsets.all(8),
              itemCount: speakersData.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: CardSpeaker(speakersData[index]),
                  onTap: () => Navigator.of(context).push(
                    PageRouteBuilder(
                      pageBuilder: (_, __, ___) =>
                          DetailSpeaker(speakersData[index]),
                      transitionsBuilder:
                          (context, animation, secondaryAnimation, child) =>
                              FadeTransition(opacity: animation, child: child),
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }
}
