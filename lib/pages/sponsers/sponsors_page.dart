import 'package:flutter/material.dart';

class SponsorsPage extends StatelessWidget {
  final assetsReal = [
    'assets/images/realizadores/udesc.jpg',
    'assets/images/realizadores/abruem.png'
  ];

  final assetsApo = [
    'assets/images/realizadores/uema.png',
    'assets/images/realizadores/unesp.jpg',
    'assets/images/realizadores/uneb.png',
    'assets/images/realizadores/unicentro.png',
    'assets/images/realizadores/upge.png',
    'assets/images/realizadores/uce.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Realizadores/Apoiadores"),
        centerTitle: true,
      ),
      body: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.grey),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Text(
                    "Realizadores",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text("Quem tornou esse evento possível!"),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _cardSponsor(assetsReal[0]),
                    _cardSponsor(assetsReal[1]),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                // _cardSponsor(assetsReal[1]),
                Text(
                  "Apoiadores",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _cardSponsor(assetsApo[0]),
                    _cardSponsor(assetsApo[1]),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _cardSponsor(assetsApo[2]),
                    _cardSponsor(assetsApo[3]),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _cardSponsor(assetsApo[4]),
                    _cardSponsor(assetsApo[5]),
                  ],
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _cardSponsor(String path) {
    return Card(
      child: Container(
        height: 150,
        width: 150,
        decoration:
            BoxDecoration(image: DecorationImage(image: AssetImage(path))),
      ),
    );
  }
}
