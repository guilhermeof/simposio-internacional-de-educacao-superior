import 'package:siies/helpers/color_hex.dart';
import 'package:flutter/material.dart';
import 'package:siies/pages/schedule/schedule_page.dart';
import 'package:siies/pages/speakers/speakers_page.dart';

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int _currentIndex = 0;
  SchedulePage schedulePage = SchedulePage();
  SpeakersPage speakersPage = SpeakersPage();
  List<Widget> _children = [];

  @override
  void initState() {
    super.initState();
    _children = [
      schedulePage,
      speakersPage,
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[_currentIndex],
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(canvasColor: Color(getColorHex('275273'))),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.shifting,
            currentIndex: _currentIndex,
            onTap: onTabTapped,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.calendar_today), title: Text("Programação")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.people), title: Text("Palestrantes")),
            ],
          ),
        ));
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
