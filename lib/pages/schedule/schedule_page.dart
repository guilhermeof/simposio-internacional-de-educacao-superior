import 'package:siies/helpers/color_hex.dart';
import 'package:siies/helpers/schedule.dart';
import 'package:siies/pages/schedule/schedule_list.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SchedulePage extends StatefulWidget {
  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  QuerySnapshot initalData;
  AppLifecycleState appLifecycleState;

  @override
  void initState() {
    super.initState();
    setState(() {
        _controller = TabController(vsync: this, length: scheduleData.length, initialIndex: 0);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: _body(scheduleData)
        );
  }

  _body(List<Schedule> snapshot) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Programação".toUpperCase(),
            style: TextStyle(),
          ),
          elevation: 7,
          centerTitle: true,
          bottom: PreferredSize(
                    preferredSize: Size.fromHeight(80.0),
                    child: Container(
                      height: 75,
                      padding: const EdgeInsets.only(bottom: 15),
                      child: TabBar(
                        indicatorSize: TabBarIndicatorSize.tab,
                        unselectedLabelColor: Colors.black54,
                        labelColor: Colors.white,
                        indicator: BoxDecoration(
                          color: Color(getColorHex('FF5747')),
                          shape: BoxShape.circle,
                        ),
                        controller: _controller,
                        isScrollable: true,
                        labelPadding: EdgeInsets.all(5),
                        tabs: snapshot.map((schedule) {
                          return Tab(
                              child: Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 30),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    schedule.dia.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  SizedBox(height: 2),
                                  Text(
                                    schedule.mes.toUpperCase(),
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ));
                        }).toList(),
                      ),
                    ),
                  ),
        ),
        body: Container(
          color: Colors.grey[50],
          child: TabBarView(
            controller: _controller,
            children: snapshot.map((schedule) {
              return ScheduleList(sessions: schedule.sessions);
            }).toList(),
          ),
        ));
  }
}
