import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 30),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Container(
            child: Column(
              children: [0, 1, 2, 3]
                  .map((_) => Padding(
                        padding: const EdgeInsets.only(bottom: 18),
                        child: Container(
                          // child: Column(
                          //   children: <Widget>[
                          //     Container(
                          //       height: 10,
                          //       width: 50,
                          //     )
                          //   ],
                          // ),
                          width: 300,
                          height: 120,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}
