import 'package:siies/helpers/color_hex.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';

class MySlider extends StatefulWidget {
  @override
  MySliderState createState() => MySliderState();
}

class MySliderState extends State<MySlider> {
  List<Slide> slides = List();

  Future<List<Slide>> _createSlides(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
          top: size.height * 0.09,
          bottom: size.height * 0.10,
        ),
        title: "Bem Vindo ao 2º SIIES",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontSize: 50,
          color: Colors.black87,
          // shadows: <Shadow>[
          //   Shadow(
          //     color: Colors.black26,
          //     blurRadius: 10.0,
          //     offset: Offset(0.0, 5.0),
          //   ),
          // ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.09,
            left: size.width * 0.05,
            right: size.width * 0.05),
        description:
            "Este aplicativo é uma forma rápida e fácil de você acessar as informações essenciais deste evento.",
        styleDescription: TextStyle(
          color: Colors.black54,
          fontSize: 20,
        ),
        pathImage: "assets/images/logo.png",

        heightImage: size.height * 0.20,
        backgroundColor: Colors.white,
        // colorBegin: Color(getColorHex('FFFFFF')),
        // colorEnd: Color(getColorHex('0098D9')),
        // directionColorBegin: Alignment.topCenter,
        // directionColorEnd: Alignment.bottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.07),
        title: "Programação",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontSize: 50,
          color: Colors.black87,
          // shadows: <Shadow>[
          //   Shadow(
          //     color: Colors.black45,
          //     blurRadius: 10.0,
          //     offset: Offset(0.0, 5.0),
          //   ),
          // ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.06,
            right: size.width * 0.06),
        description:
            "Você pode visualizar a programação do evento, saiba tudo sobre as atividades que deseja acompanhar.",
        styleDescription: TextStyle(
          color: Colors.black54,
          fontSize: 20.0,
        ),
        pathImage: "assets/images/intro/2.png",
        heightImage: size.height * 0.35,
        backgroundColor: Colors.white,
        // colorBegin: Color(getColorHex('0CE7F0')),
        // colorEnd: Color(getColorHex('0098D9')),
        // directionColorBegin: Alignment.topCenter,
        // directionColorEnd: Alignment.bfottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.07),
        title: "Palestrantes",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontSize: 50,
          color: Colors.black87,
          // shadows: <Shadow>[
          //   Shadow(
          //     color: Colors.black26,
          //     blurRadius: 10.0,
          //     offset: Offset(0.0, 5.0),
          //   ),
          // ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.06,
            right: size.width * 0.06),
        description:
            "Confira quem vai marcar presença na programação, especialmente montada com o que há de melhor na área.",
        styleDescription: TextStyle(
          color: Colors.black54,
          fontSize: 20.0,
        ),
        pathImage: "assets/images/intro/3.png",
        heightImage: size.height * 0.35,
        backgroundColor: Colors.white,
        // colorBegin: Color(getColorHex('0CE7F0')),
        // colorEnd: Color(getColorHex('0098D9')),
        // directionColorBegin: Alignment.topCenter,
        // directionColorEnd: Alignment.bottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.05),
        title: "Tenha um bom Evento!",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontSize: 50,
          color: Colors.black87,
          // shadows: <Shadow>[
          //   Shadow(
          //     color: Colors.black45,
          //     blurRadius: 10.0,
          //     offset: Offset(0.0, 5.0),
          //   ),
          // ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.04,
            right: size.width * 0.04),
        description:
            "UDESC, em nome de toda a equipe da organização, deseja um ótimo evento!",
        styleDescription: TextStyle(
          color: Colors.black54,
          fontSize: 20.0,
          fontFamily: 'Raleway',
        ),
        pathImage: "assets/images/intro/4.png",
        heightImage: size.height * 0.32,
        backgroundColor: Colors.white,
        // colorBegin: Color(getColorHex('0CE7F0')),
        // colorEnd: Color(getColorHex('0098D9')),
        // directionColorBegin: Alignment.topCenter,
        // directionColorEnd: Alignment.bottomCenter,
      ),
    );

    return Future.value(slides);
  }

  void onDonePress() {
    // Do what you want
    Navigator.pushReplacementNamed(context, '/home');
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Colors.black45,
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Colors.black45,
    );
  }

  Widget renderSkipBtn() {
    return Text(
      'Pular',
      style: TextStyle(
        color: Colors.black45,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Slide>>(
      future: _createSlides(context),
      builder: (BuildContext context, AsyncSnapshot<List<Slide>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            return IntroSlider(
              // List slides
              slides: this.slides,

              // Skip button
              renderSkipBtn: this.renderSkipBtn(),

              // Next button
              renderNextBtn: this.renderNextBtn(),

              // Done button
              renderDoneBtn: this.renderDoneBtn(),
              onDonePress: this.onDonePress,

              // Dot indicator
              colorDot: Colors.black45,
              colorActiveDot: Color(getColorHex('F58634')),
              sizeDot: 13.0,

              // Show or hide status bar
              shouldHideStatusBar: false,
            );
        }
        return null; // unreachable
      },
    );
  }
}
