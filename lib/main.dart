import 'package:siies/helpers/color_hex.dart';
import 'package:siies/pages/home/home_page.dart';
import 'package:siies/pages/intro/slider.dart';
import 'package:siies/pages/intro/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(AppEvent());

class AppEvent extends StatefulWidget {
  _AppEventState createState() => _AppEventState();
}

class _AppEventState extends State<AppEvent> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SIIES',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Color(getColorHex('0098DA')),
        primaryColor: Colors.white,
        primarySwatch: Colors.green
      ),
      routes: {
        '/splash': (_) => SplashPage(),
        '/tutorial': (_) => MySlider(),
        '/home': (_) => HomePage(),
      },
      home: SplashPage(),
      // home: HomePage(),
    );
  } 
}
