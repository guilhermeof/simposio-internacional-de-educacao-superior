import 'package:cloud_firestore/cloud_firestore.dart';

class Speaker {
  final String id;
  final String nome;
  final String descricao;
  final String local;
  final String pais;
  final String foto;

  Speaker(
      {this.id, this.nome, this.descricao, this.local, this.pais, this.foto});

  factory Speaker.fromDb(DocumentSnapshot snapshot) {
    return Speaker(
        id: snapshot['id'],
        nome: snapshot['nome'],
        descricao: snapshot['descricao'],
        local: snapshot['local'],
        pais: snapshot['pais'],
        foto: snapshot['foto']);
  }
}

List<Speaker> speakersData = [];

Future<QuerySnapshot> _getSpeakers() async {
  var firestore = Firestore.instance;

  QuerySnapshot querySnapshot =
      await firestore.collection('speakers').orderBy('nome').getDocuments();

  return querySnapshot;
}

void allSpeakers() async {
  QuerySnapshot querySnapshot = await _getSpeakers();

  speakersData = querySnapshot.documents.map((data) {
    return Speaker.fromDb(data);
  }).toList();
}
