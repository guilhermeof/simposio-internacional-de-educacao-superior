class Curso {
  String name;
  String duration;

  Curso({this.name, this.duration});
}

final cursos = [
  // Oferta(
  Curso(name: "Administração Bacharelado", duration: "4,5"),
  Curso(name: "Agronomia Bacharelado", duration: "5"),
  Curso(name: "Arquitetura e Urbanismo Bacharelado", duration: "5"),
  Curso(name: "Ciências Sociais Licenciatura", duration: "4"),
  Curso(name: "Ciências Sociais Bacharelado", duration: "4"),
  Curso(name: "Ciências Naturais Licenciatura", duration: "4"),
  Curso(name: "Ciências Contábeis Bacharelado", duration: "4"),
  Curso(name: "Ciências Biológicas Bacharelado", duration: "4"),
  Curso(name: "Ciências Biológicas Licenciatura", duration: "4"),
  Curso(name: "Ciências Licenciatura em Biologia", duration: "4"),
  Curso(name: "Curso Superior de Tecnologia em Gestão Ambiental", duration: "2,5"),
  Curso(name: "Curso Superior de Tecnologia em Gestão Comercial", duration: "2"),
  Curso(name: "Curso de Formação de Oficiais Bombeiro – Militar Bacharelado em Segurança Pública e do trabalho", duration: "3"),
  Curso(name: "Curso Superior de Tecnologia em Fruticultura", duration: "2,5"),
  Curso(name: "Curso Superior de Tecnologia em Alimentos", duration: "3"),
  Curso(name: "Curso Superior de Tecnologia em Gestão do Agronegócio", duration: "3"),
  Curso(name: "Curso de Formação de Oficias da Policia Militar – (CFO-PM/UEMA)-Bacharelado em Segurança Pública", duration: "3,5"),
  Curso(name: "Direito Bacharelado", duration: "5"),
  Curso(name: "Educação Física Licenciatura", duration: "4"),
  Curso(name: "Enfermagem Bacharelado", duration: "5"),
  Curso(name: "Engenharia Civil Bacharelado", duration: "5"),
  Curso(name: "Engenharia de Pesca Bacharelado", duration: "5,5"),
  Curso(name: "Engenharia Mecânica Bacharelado", duration: "4,5"),
  Curso(name: "Engenharia da Computação Bacharelado", duration: "4,5"),
  Curso(name: "Engenharia de Produção Bacharelado", duration: "5"),
  Curso(name: "Filosofia Licenciatura", duration: "4"),
  Curso(name: "Física Licenciatura", duration: "4"),
  Curso(name: "Geografia Licenciatura - (EAD)", duration: "4"),
  Curso(name: "Geografia Licenciatura", duration: "4"),
  Curso(name: "Geografia Bacharelado", duration: "4"),
  Curso(name: "História Licenciatura", duration: "4"),
  Curso(name: "Letras Licenciatura em Língua Portuguesa, Língua Espanhola e Literaturas", duration: "4,5"),
  Curso(name: "Letras Licenciatura em Língua Portuguesa , Língua Inglesa e Literaturas", duration: "4,5"),
  Curso(name: "Letras Licenciatura em Língua Portuguesa e Literaturas de Língua Portuguesa", duration: "4,5"),
  Curso(name: "Licenciatura Intercultural para Educação Básica Indígena", duration: " "),
  Curso(name: "Medicina Bacharelado", duration: "6"),
  Curso(name: "Medicina Veterinária Bacharelado", duration: "5"),
  Curso(name: "Matemática Licenciatura", duration: "4"),
  Curso(name: "Música Licenciatura", duration: "4"),
  Curso(name: "Pedagogia Licenciatura", duration: "4"),
  Curso(name: "Química Licenciatura", duration: "4"),
  Curso(name: "Zootecnia Bacharelado", duration: "4")
];
