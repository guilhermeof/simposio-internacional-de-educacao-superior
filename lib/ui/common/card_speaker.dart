import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:siies/models/speakers.dart';
import 'package:siies/ui/detail/detail_speaker.dart';
import 'package:siies/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class CardSpeaker extends StatelessWidget {
  final Speaker speaker;

  CardSpeaker(this.speaker);

  @override
  Widget build(BuildContext context) {
    final leftSection = Container(
      child: Hero(
        tag: "speaker-hero-${speaker.id}",
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Image(
            fit: BoxFit.cover,
            image: AdvancedNetworkImage(
              speaker.foto,
              useDiskCache: true,
              cacheRule: CacheRule(
                maxAge: const Duration(days: 1),
              ),
            ),
            height: 60.0,
            width: 60.0,
          ),
        ),
      ),
    );

    final middleSection = Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              speaker.nome,
              style: Style.commonTextStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                Text(
                  speaker.local,
                  style: Style.smallTextSpeakerListCard,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                Container(
                  height: 20,
                  child: SvgPicture(AdvancedNetworkSvg(
                      speaker.pais, SvgPicture.svgByteDecoder,
                      useDiskCache: true,
                      cacheRule: CacheRule(maxAge: const Duration(days: 1)))),
                )
              ],
            ),
          ],
        ),
      ),
    );

    final cardAreaContent = Container(
        margin: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[leftSection, middleSection],
        ));

    final speakerCard = Container(
      child: cardAreaContent,
      height: 80.0,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.black, style: BorderStyle.solid),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 5.0),
          ),
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => DetailSpeaker(speaker)));
      },
      child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 5.0,
          ),
          child: speakerCard),
    );
  }
}
