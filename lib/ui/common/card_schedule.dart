import 'package:siies/helpers/schedule.dart';
import 'package:siies/ui/detail/detail_schedule.dart';
import 'package:siies/ui/styles/text_style.dart';
import 'package:flutter/material.dart';
import 'package:siies/ui/common/separator.dart';

class CardSchedule extends StatelessWidget {
  final Sessions schedule;
  final bool horizontal;

  CardSchedule(this.schedule, {this.horizontal = true});

  CardSchedule.vertical(this.schedule) : horizontal = false;

  @override
  Widget build(BuildContext context) {

    Widget _scheduleValue({String value, IconData icon}) {
      return Container(
        child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Icon(icon, size: 16.0),
          SizedBox(width: 8.0),
          Text(
            value,
            style: Style.smallTextStyle,
          ),
        ]),
      );
    }

    final schedulecardAreaContent = Container(
      margin: EdgeInsets.fromLTRB(
          horizontal ? 25.0 : 16.0, horizontal ? 16.0 : 20.0, 16.0, 16.0),
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment:
            horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        children: <Widget>[
          Text(schedule.nome, style: Style.commonTextStyle, maxLines: 2, overflow: TextOverflow.ellipsis,),
          Separator(),
          SizedBox(height: horizontal ? 0 : 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: horizontal ? 1 : 0,
                  child: _scheduleValue(
                    icon: Icons.timer,
                    value: schedule.inico,
                  )),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                  flex: horizontal ? 1 : 0,
                  child: _scheduleValue(
                    icon: Icons.timer,
                    value: schedule.termino,
                  ))
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              schedule.local.isEmpty ? Container() : _scheduleValue(
                icon: Icons.pin_drop,
                value: schedule.local,
              )
            ],
          ),
          // SizedBox(height: 10,),
          
        ],
      ),
    );

    final scheduleCard = Container(
      child: schedulecardAreaContent,
      alignment: Alignment.center,
      height: horizontal ? 124.0 : 154.0,
      margin:
          horizontal ? EdgeInsets.only(left: 0.0) : EdgeInsets.only(top: 72.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.black, style: BorderStyle.solid),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 5.0),
          ),
        ],
      ),
    );

    return GestureDetector(
        onTap: horizontal
            ? () => Navigator.of(context).push(
                  PageRouteBuilder(
                    pageBuilder: (_, __, ___) => DetailSchedule(schedule),
                    transitionsBuilder:
                        (context, animation, secondaryAnimation, child) =>
                            FadeTransition(opacity: animation, child: child),
                  ),
                )
            : null,
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 15.0,
          ),
          child: scheduleCard
        ));
  }
}
