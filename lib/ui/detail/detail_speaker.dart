import 'package:siies/helpers/color_hex.dart';
import 'package:siies/models/speakers.dart';
import 'package:siies/ui/common/card_datail_speaker.dart';
import 'package:flutter/material.dart';
import 'package:siies/ui/common/separator.dart';
import 'package:siies/ui/styles/text_style.dart';


class DetailSpeaker extends StatelessWidget {

  final Speaker speaker;

  DetailSpeaker(this.speaker);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Color(getColorHex('FFFFFF')),
        child: SingleChildScrollView(
          child: Stack (
          children: <Widget>[
            _getBackground(),
            _getGradient(),
            _getContent(),
            _getToolbar(context),
          ],
        ),
        )
      ),
    );
  }

  Container _getBackground () {
    return Container(
     child: Image.asset('assets/images/card_2.jpg',
       fit: BoxFit.cover,
       height: 300.0,
     ),
      constraints: BoxConstraints.expand(height: 295.0),
    );
  }

  Container _getGradient() {
    return Container(
      margin: const EdgeInsets.only(top: 190.0),
      height: 110.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color(getColorHex('FFFFFF')).withOpacity(0.0),
            Color(getColorHex('FFFFFF')),
          ],
          stops: [0.0, 0.9],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 1.0),
        ),
      ),
    );
  }

  Container _getContent() {
    print(speaker.descricao);
    final _overviewTitle = "Sobre".toUpperCase();
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 72.0, 0.0, 32.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CardDetailSpeaker(speaker,
            horizontal: false,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(_overviewTitle,
                  style: Style.headerTextStyle,),
                Separator(),
                Text(
                    speaker.descricao, style: Style.commonTextStyle),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _getToolbar(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: MediaQuery
              .of(context)
              .padding
              .top),
      child: BackButton(color: Colors.white),
    );
  }
}