import 'package:siies/helpers/color_hex.dart';
import 'package:siies/helpers/schedule.dart';
import 'package:siies/ui/common/card_schedule.dart';
import 'package:flutter/material.dart';
import 'package:siies/ui/common/separator.dart';
import 'package:siies/ui/styles/text_style.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailSchedule extends StatelessWidget {
  final Sessions schedule;

  DetailSchedule(this.schedule);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Color(getColorHex('FF9F6E')),
        child: Stack(
          children: <Widget>[
            _getBackground(),
            _getGradient(),
            _getContent(),
            _getToolbar(context),
          ],
        ),
      ),
    );
  }

  Container _getBackground() {
    return Container(
      child: Image.asset(
        'assets/images/card_1.jpg',
        fit: BoxFit.cover,
        height: 300.0,
      ),
      constraints: BoxConstraints.expand(height: 295.0),
    );
  }

  Container _getGradient() {
    return Container(
      margin: EdgeInsets.only(top: 190.0),
      height: 110.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color(getColorHex('FF9F6E')).withOpacity(0.0),
            Color(getColorHex('FF9F6E')),
          ],
          stops: [0.0, 0.9],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 1.0),
        ),
      ),
    );
  }

  Container _getContent() {
    final _overviewTitle = "Descrição".toUpperCase();
    return Container(
      child: ListView(
        padding: EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
        children: <Widget>[
          CardSchedule(
            schedule,
            horizontal: false,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  _overviewTitle,
                  style: Style.headerTextStyle,
                ),
                Separator(),
                Text(schedule.descricao, style: Style.commonTextStyle),
                schedule.nome == "COMUNICAÇÕES ORAIS"
                    ? 
                    Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: RaisedButton(
                        color: Colors.white,
                        onPressed: _launchURL,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Programação'.toUpperCase(),
                              style: TextStyle(color: Colors.black87),
                            ),
                            SizedBox(width: 5),
                            Icon(
                              Icons.file_download,
                              color: Colors.black87,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                    )
                    : Container()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _getToolbar(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: new BackButton(color: Colors.white),
    );
  }
}

Future<dynamic> _launchURL() async {
  const url =
      'http://siies2019.udesc.br/wp-content/uploads/2019/09/Programa%C3%A7%C3%A3o-Apresenta%C3%A7%C3%B5es.pdf';      
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
