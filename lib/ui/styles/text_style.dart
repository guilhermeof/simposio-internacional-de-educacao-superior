import 'package:flutter/material.dart';

class Style {
  static final baseTextStyle = const TextStyle(
      fontFamily: 'Poppins'
  );
  static final smallTextStyle = commonTextStyle.copyWith(
    fontSize: 9.0,
  );
  static final smallTextSpeakerListCard = commonTextStyle.copyWith(
    fontSize: 13,
    color: Colors.grey[700]
  );
  static final commonTextStyle = baseTextStyle.copyWith(
      color: Colors.black87,
      fontSize: 14.0,
      fontWeight: FontWeight.w400
  );
  static final cardareastyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 14.0,
      fontWeight: FontWeight.w400
  );
  static final speakerTitleTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 14.0,
      fontWeight: FontWeight.w400
  );
  static final titleTextStyle = baseTextStyle.copyWith(
      color: Colors.black87,
      fontSize: 18.0,
      fontWeight: FontWeight.w600
  );
  static final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.black87,
      fontSize: 20.0,
      fontWeight: FontWeight.w400
  );
}